import os.path

import subprocess
import glob
import math
from os import remove

from Rutas import *
from Rutas_Dir import *
from File_Data_hypDD import *
from station import *
#from In_Out_HypDD import *
#from Principal import *

from files_ct_prom_hyp import *
from Data_Times_HypDD import *



class file_hypodd():

	def menu(self, name):
		self.model=name
		print (self.model)
		print("1.- Create file to dat (.dat)")
		print("2.- Create file to station(.sta)")
		print("3.- Create file of data times")
		print("4.- Create file dt.ct")
		print("5.- Create file dt.cc")
		print("6.- Create file of settings(inp)")
		print("7.- System locations using HypoDD")
		print("8.- exit")
		choice=input("Select choice (1-8):\n")
		
		
		

		try:
			
			choice=int(choice)
			print ("choice select: ",choice)
			if choice <=0 or choice>=9:
				os.system("clear")
				print("Choice incorrect, please select choice between 1 and 8\n")

		except:
			os.system("clear")
			choice=0
			print("Choice incorrect, please enter number between 1 and 8\n")


		while choice <=0 or choice>=9:

			print("1.- Create file to dat (.dat)")
			print("2.- Create file to station(.sta)")
			print("3.- Create file of data times")
			print("4.- Create file dt.ct")
			print("5.- Create file dt.cc")
			print("6.- Create file of settings(inp)")
			print("7.- System locations using HypoDD")
			print("8.- exit")
			choice=input("Select choice (1-8):\n")
			try:
				
				choice=int(choice)
				print ("choice select: ",choice)
				if choice <=0 or choice>=9:
					os.system("clear")
					print("Choice incorrect, please select choice between 1 and 8\n")
			except:
				os.system("clear")
				choice=0
				print("Choice incorrect, please select choice between 1 and 8\n")
		


		if choice == 1:
			self.file_data()
		if choice == 2:
			self.station()
		if choice == 3:
			self.data_times()
		if choice == 4:
			self.files_ct()
		if choice == 5:
			self.files_cc()
		if choice == 6:
			self.settings()
		if choice == 7:
			self.HypoDD()
		if choice == 8:
			os.system("clear")
			#iniciar=Principal()
			#iniciar.main()
			#print("termina")
		

	def settings(self):
		print (' ******* settings')
		print ("inp")

		
		self.create_file()
		
		print("process completed successfully")
		self.menu(self.INPUT)


	def HypoDD(self):
		print ('******* HypoDD')
		print ("loc")
		self.Software_hypoDD()
		
		print("process completed successfully")
		self.menu(self.INPUT)



	def file_data(self):
		print ('  ******* file_data')
		
		inicio_data_dat=File_Data_hypDD()
		
		Tipo="File with localization"
		path_loca=input("Input path file localization:\n")


		try:
			if (os.path.isfile(path_loca)) :
				status_loc=True
		except Exception as e:
			print("Error, format incorrect")
			status_loc=False

		while status_loc== False:
			path_loca=input("Input file .crh")

		

			try:
				if (os.path.isfile(path_loca)) :
					status_loc=True
			except Exception as e:
				print("Error, format incorrect")
				status_loc=False
		
		inicio_data_dat.main(path_loca,self.model)
		
		print("process completed successfully")
		self.menu(self.INPUT)


	def data_times(self):
		print (' ******* data_times')
		
		opc=1
		print ("manda a llamar la funcion datos_time_hyp")
		inicio_times = Data_Times_HypDD()
		#opc=1
		#model="codex_16"
		inicio_times.main_data_times_hyp(self.model, opc)
		#self.main_data_times_hyp(self.model, opc)
		self.end_data_times()


	def end_data_times(self):
		
		print("process completed successfully")
		self.menu(self.INPUT)
		

		
		#print("process completed successfully")
		#self.menu(self.INPUT)

	def files_ct(self):
		print (' ******* dt.ct')
		
		inicio_files_ct=files_ct_prom_hypdd()
		#model="codex_17"
		inicio_files_ct.main_files_ct(self.model)

		
		print("process completed successfully")
		self.menu(self.INPUT)


	def files_cc(self):
		print (' ******* dt.cc')
		
		#inicio_in_out=In_Out_HypDD()
		self.Menu_cc()
		
		print("process completed successfully")
		self.menu(self.INPUT)
	def station(self):
		print ('  ******* station')
		
		inicio_data_sta=station()
		#inicio_ruta=Rutas()
		#Tipo="File with station"
		#path_loca=inicio_ruta.main(Tipo)

		self.path_sta=input("Input path file stations")

		

		try:
			if (os.path.isfile(self.path_sta)) :
				status_sta=True
		except Exception as e:
			print("Error, format incorrect")
			status_sta=False

		while status_sta== False:
			self.path_sta=input("Input path file stations")

		

			try:
				if (os.path.isfile(self.path_sta)) :
					status_sta=True
			except Exception as e:
				print("Error, format incorrect")
				status_sta=False




		inicio_data_sta.main(self.path_sta,self.model)
		
		print("process completed successfully")
		self.menu(self.INPUT)


	

	def Take_input(self):

		#self.INPUT = self.inputtxt.get("1.0", "end-1c")
		print(self.INPUT)
		a=0
		if self.INPUT!="":
			self.menu(self.INPUT)
			
	


	def main(self):
		os.system("clear")
		print ("Bienvenidos al Software hypoDD con python")
		
		self.INPUT=input("Input name to identifier:\n")


		if self.INPUT!="":
			self.menu(self.INPUT)
		else:
			print ("Termina Programa")
			os.system("clear")



	def Lectura_file(self):
		#inicio_ruta=Rutas()
		Tipo="Input file .crh"
		path=input("Input file .crh")
		try:
			if (os.path.isfile(path)) :
				status_crh=True
		except Exception as e:
			print("Error, format incorrect")
			status_crh=False

		while status_crh== False:
			path=input("Input file .crh")

		

			try:
				if (os.path.isfile(path)) :
					status_crh=True
			except Exception as e:
				print("Error, format incorrect")
				status_crh=False
		#path="/home/carlos/Dropbox/hyp/codex/"+self.model+".crh"
		f = open(path,"r")
		arreglo= []
		contLin=0
		while True:
			linea = f.readline()
			if linea:
				datos = linea.split('\n')
				datos = datos[0].split(' ')
				arreglo.append(datos)
			else:
				break

		return arreglo

	def Software_hypoDD(self):

		print ("Entra a la funcion de Software")
		self.name_soft=(os.path.abspath(os.getcwd()))

		#p=""
		p = subprocess.Popen(['./hypoDD'],
                     			stdout = subprocess.PIPE,
                     			stdin  = subprocess.PIPE,
                     			stderr = subprocess.STDOUT,
                     		encoding="utf8" )

			
		s=''
		line1=self.name_soft+'/Files_HypDD/'+self.model+'.inp\n'
		s+=line1
		print (s	)

		out = p.communicate( s )
		print (out	)


	def Menu_cc(self):
		
		rsp=input("using settings with con file dt.cc Y/n:\n")


		try:
			if rsp=="y" or rsp=="Y" or rsp=="yes" or rsp =="YES" or rsp =="n" or rsp=="N" or rsp=="not" or rsp=="NOT":
				status_rsp=True
		except Exception as e:
			print("Error, please select choice Y/n")
			status_rsp=False

		while status_rsp == False:
			rsp=input("using settings with con file dt.cc  Y/n:\n")


			try:
				if rsp=="y" or rsp=="Y" or rsp=="yes" or rsp =="YES" or rsp =="n" or rsp=="N" or rsp=="not" or rsp=="NOT":
					status_rsp=True
			except Exception as e:
				print("Error, please select choice Y/n")
				status_rsp=False
		


		if rsp=="y" or rsp=="Y" or rsp=="yes" or rsp =="YES":
			self.file_cc_yes()
		else:
			self.file_cc_not()
	def file_cc_yes(self):
		
		Tipo="Select file with dt.cc"
		self.file_cc=input("Select file with dt.cc")
		self.opc_cc=1
		self.Menu_IDAT()


	def file_cc_not(self):
		self.opc_cc=0
		self.Menu_IDAT()


	def Menu_IDAT(self):
		

		print("1.- 0 = synthetics")
		print("2.- 1= cross corr")
		print("3.- 2= catalog")
		print("4.- 3= cross & cat")
		choice=input("Select choice (1-4):\n")
		
		
		

		try:
			
			choice=int(choice)
			print ("choice select: ",choice)
			if choice <=0 or choice>=5:
				os.system("clear")
				print("Choice incorrect, please select choice between 1 and 4\n")

		except:
			os.system("clear")
			choice=0
			print("Choice incorrect, please enter number between 1 and 4\n")


		while choice <=0 or choice>=5:

			print("1.- 0 = synthetics")
			print("2.- 1= cross corr")
			print("3.- 2= catalog")
			print("4.- 3= cross & cat")
			choice=input("Select choice (1-4):\n")
			try:
				
				choice=int(choice)
				print ("choice select: ",choice)
				if choice <=0 or choice>=4:
					os.system("clear")
					print("Choice incorrect, please select choice between 1 and 4\n")
			except:
				os.system("clear")
				choice=0
				print("Choice incorrect, please select choice between 1 and 4\n")
		


		if choice == 1:
			self.settings_idat(opc=0)
		if choice == 2:
			self.settings_idat(opc=1)
		if choice == 3:
			self.settings_idat(opc=2)
		if choice == 4:
			self.settings_idat(opc=3)

	def settings_idat(self, opc):
		self.opc_idat=str(opc)
		
		self.Menu_IPHA()

	def Menu_IPHA(self):
		
		print("1.- 1 = p")
		print("2.- 2= s")
		print("3.- 3= p & s")
		choice=input("Select choice (1-3):\n")
		
		
		

		try:
			
			choice=int(choice)
			print ("choice select: ",choice)
			if choice <=0 or choice>=4:
				os.system("clear")
				print("Choice incorrect, please select choice between 1 and 3\n")

		except:
			os.system("clear")
			choice=0
			print("Choice incorrect, please enter number between 1 and 3\n")


		while choice <=0 or choice>=5:

			print("1.- 1 = p")
			print("2.- 2= s")
			print("3.- 3= p & s")
			choice=input("Select choice (1-3):\n")
			try:
				
				choice=int(choice)
				print ("choice select: ",choice)
				if choice <=0 or choice>=4:
					os.system("clear")
					print("Choice incorrect, please select choice between 1 and 3\n")
			except:
				os.system("clear")
				choice=0
				print("Choice incorrect, please select choice between 1 and 3\n")
		


		if choice == 1:
			self.settings_ipha(opc=1)
		if choice == 2:
			self.settings_ipha(opc=2)
		if choice == 3:
			self.settings_ipha(opc=3)
		

	def settings_ipha(self, opc):
		self.opc_ipha=str(opc)
		#self.create_file()
		self.create_file_continue()


	def create_file(self):

		Tipo="path salve archivo .inp"
		self.path_create_file=os.getcwd()
		
		self.files_ct=input("Select file with dt.ct:\n")
		print ("---+++++++++++",self.files_ct)
		self.Menu_cc()
	def create_file_continue(self):
		#self.Menu_IDAT()

		#self.Menu_IPHA()



		#self.name_salve_file=os.getcwd()+"/Files_HypD/"+self.model+"/"
		self.name_salve_file=os.getcwd()
		self.sta_dat=self.name_salve_file+'/Files_HypDD/'+self.model+'_sta.dat'
		self.data_dat=self.name_salve_file+'/Files_HypDD/'+self.model+'.dat'
		datos_model=self.Lectura_file()
		for m in range(2):

			if m==0:

				fo = open(self.path_create_file+'/'+self.model+'.inp', 'w')
			else:
				fo = open(self.name_salve_file+'/Files_HypDD/'+self.model+'.inp', 'w')
			fo.write("* RELOC.INP:\n")
			fo.write("*--- input file selection\n")
			fo.write("* cross correlation diff times: (not used in this test)\n")
			if self.opc_cc==1:
				fo.write(self.files_cc+"\n")
			else:
				fo.write("#dt.cc\n")


			
			fo.write("*\n")
			fo.write("*catalog P & S diff times:\n")
			fo.write(self.files_ct+"\n")
			fo.write("*\n")
			fo.write("* event file:\n")
			fo.write(self.data_dat+"\n")
			fo.write("*\n")
			fo.write("* station file:\n")
			fo.write(self.sta_dat+"\n")
			fo.write("*\n")
			fo.write("*--- output file selection\n")
			fo.write("* original locations:\n")
			fo.write(self.path_create_file+"/"+self.model+".loc\n")
			fo.write("* relocations:\n")
			fo.write(self.path_create_file+"/"+self.model+".reloc\n")
			fo.write("* station information:\n")
			fo.write(self.path_create_file+"/"+self.model+".sta\n")
			fo.write("* residual information:\n")
			fo.write("*"+self.model+".res\n")
			fo.write("\n")
			fo.write("* source paramater information:\n")
			fo.write("*"+self.model+".src\n")
			fo.write("\n")
			fo.write("*\n")
			fo.write("*--- data type selection: \n")
			fo.write("* IDAT:  0 = synthetics; 1= cross corr; 2= catalog; 3= cross & cat \n")
			fo.write("* IPHA: 1= P; 2= S; 3= P&S\n")
			fo.write("* DIST:max dist [km] between cluster centroid and station \n")
			fo.write("* IDAT   IPHA   DIST\n")
			fo.write("    "+self.opc_idat+"     "+self.opc_ipha+"     400\n")
			fo.write("*\n")
			fo.write("*--- event clustering:\n")
			fo.write("* OBSCC:    min # of obs/pair for crosstime data (0= no clustering)\n")
			fo.write("* OBSCT:    min # of obs/pair for network data (0= no clustering\n")
			fo.write("* OBSCC  OBSCT    \n")
			fo.write("     2     2      \n")
			fo.write("*\n")
			fo.write("*--- solution control:\n")
			fo.write("* ISTART:  	1 = from single source; 2 = from network sources\n")
			fo.write("* ISOLV:	1 = SVD, 2=lsqr\n")
			fo.write("* NSET:      	number of sets of iteration with specifications following\n")
			fo.write("*  ISTART  ISOLV  NSET\n")
			fo.write("    1        2      2\n")
			fo.write("*\n")
			fo.write("*--- data weighting and re-weighting: \n")
			fo.write("* NITER: 		last iteration to use the following weights\n")
			fo.write("* WTCCP, WTCCS:		weight cross P, S \n")
			fo.write("* WTCTP, WTCTS:		weight catalog P, S \n")
			fo.write("* WRCC, WRCT:		residual threshold in sec for cross, catalog data \n")
			fo.write("* WDCC, WDCT:  		max dist [km] between cross, catalog linked pairs\n")
			fo.write("* DAMP:    		damping (for lsqr only) \n")
			fo.write("*       ---  CROSS DATA ----- ----CATALOG DATA ----\n")
			fo.write("* NITER WTCCP WTCCS WRCC WDCC WTCTP WTCTS WRCT WDCT DAMP\n")
			fo.write("  5      -9     -9   -9   -9   1.0    -9  -9    -9   20\n")
			fo.write("  5      -9     -9   -9   -9   1.0    -9   5     8   20\n")
			fo.write("*\n")
			fo.write("*--- 1D model:\n")
			fo.write("* NLAY:		number of model layers  \n")
			fo.write("* RATIO:	vp/vs ratio \n")
			fo.write("* TOP:		depths of top of layer (km) \n")
			fo.write("* VEL: 		layer velocities (km/s)\n")
			fo.write("* NLAY  RATIO \n")

			#datos_model=self.Lectura_file()

			fo.write("   "+str(len(datos_model)-1)+"     1.73\n")
			#fo.write("*Colima model(jimenez, 2012). Depth to top, velocity\n")
			print (datos_model[0])
			fo.write("*"+str(datos_model[0])+". Depth to top, velocity\n")
			fo.write("* TOP \n")

			#print datos_model
			#fo.write("*********Datos nuevos****\n")
			for i in range(1,len(datos_model)):
				#i=i+1
				aux=[]
				for j in datos_model[i]:
					if j!="":
						aux.append(j)
				datos_model[i]=aux
	
				#print datos_model[i]
				print (datos_model[i][1]," ",)
			
				fo.write(str(datos_model[i][1]+" "))
			fo.write("\n")
			#fo.write("********* Fin Datos nuevos****\n")
			print ("\n")



			#fo.write("0.0 2.0 4.0 6.0 12.0 18.0 35.0\n")
			fo.write("* VEL\n")
			for i in range(1,len(datos_model)):
				#i=i+1
				aux=[]
				for j in datos_model[i]:
					if j!="":
						aux.append(j)
				datos_model[i]=aux
	
				#print datos_model[i]
				print (datos_model[i][0]," ",)
				
				fo.write(str(datos_model[i][0]+" "))
			fo.write("\n")
			print ("\n")
			#fo.write("		
			#fo.write("1.90 3.40 3.80 5.90 6.00 7.40 8.10\n")
			fo.write("*\n")
			fo.write("*--- event selection:\n")
			fo.write("* CID: 	cluster to be relocated (0 = all)\n")
			fo.write("* ID:	ids of event to be relocated (8 per line)\n")
			fo.write("* CID    \n")
			fo.write("    0      \n")
			fo.write("* ID")

	def main_data_times_hyp(self, mode, opc):
		#model='codex_16'
		self.opc=opc
		self.model=mode
		print ("Bienvenidos al Software file input and out file HypoDD con python")

		print ("Ingresa el nombre del modelo a ser analizado:")
		#model=""
		#model=raw_input()
		#print type(self.model)
		self.archivos_origen=self.origen(self.model)
		#print "****orig"
		self.archivos_origen.pop(0)
		#for i in self.archivos_origen:
			#print i
		pregunta_b=0
		self.main_pregunta(pregunta_b)

	def datos_proceso(self, archivos_pick):
		#archivos_pick=self.Lec_files()

		#archivos_origen=self.origen(model)
		#print "****origen"
		#archivos_pick=self.Read_files()

		print( "****read")
		
		
		#print len(archivos_pick)
		#print len(archivos_cross)
		print ("Nombre del modelo:  ", self.model)
		inicio_rutas_dir=Rutas_Dir()
		Tipo="Input path to salve files times"
		path_salve=inicio_rutas_dir.pathfile(Tipo)
		path_salve=path_salve+'/datos_time/'+self.model+'/'
		for i in range(len(self.archivos_origen)):
			idece_analizar=int(self.archivos_origen[i][0])
			self.Proceso(archivos_pick[idece_analizar], self.archivos_origen[i], self.model, path_salve )

		print ("************************")
		print ("Fin de proceso uno")
		print ("************************")


		self.end_data_times()
		#print("process completed successfully")
		#inicio_hypDD=file_hypodd()
		#inicio_hypDD.data_times(opc=2)
	
						
		#resp=raw_input("Deseas realizar otra proceso (Yes), ingresa cualquier valor y enter para terminar:  ")

	def origen(self, model):	
		#model='codex_13'
		inicio_rutas=Rutas()
		Tipo="Input path "+model+" localization"
		ruta_loca=inicio_rutas.main(Tipo)

		#ruta_loca='/home/carlos/Dropbox/Catalogo_2006_2007/'+model+'.txt'
		f = open(ruta_loca,"r")
		elementos= []
		while True:
			linea = f.readline()
			if linea:
				elementos.append(linea)
			else:
						break

		elementos1 = []
		for i in elementos:
			lista=i.split("\t")
			lista.remove("\n")
			elementos1.append([(e) for e in lista])
		
		archivos=[]


		#for i in elementos1:
			#aux=[]
			#print i[0], i[2], i[3]
		
		return elementos1	
	def main_pregunta(self, b):
		if b==0:
			self.a=[]
			self.pregunta()
		elif b==1:
			self.pregunta()
		else:
			#print self.a
			self.Lec_files(self.a)

	def Pregunta_yes(self):
	    

	    file=input("Input Path Directory whit files in phases:\n ")
	    try:
	    	if (os.path.isdir(file)) :
	    		
	    		status_df=True
	    except Exception as e:
	    	print("Error, format incorrect")
	    	status_df=False
	    while status_df==False:
	    	print("path not is a dir")
	    	file=input("Input Path Directory whit files in phases:\n ")
	    	try:
	    		if (os.path.isdir(file)) :
	    			
	    			status_df=True
	    	except Exception as e:
	    		print("Error, format incorrect")
	    		status_df=False

	    self.a.append(file+'/')
	    b=1
	    self.main_pregunta(b)


	def Pregunta_not(self):
	    
	    b=2
	    self.main_pregunta(b)


	def pregunta(self):

		a=[]
		

		rsp=input(" Select path of the files Y/n:\n")


		try:
			if rsp=="y" or rsp=="Y" or rsp=="yes" or rsp =="YES" or rsp =="n" or rsp=="N" or rsp=="not" or rsp=="NOT":
				status_rsp=True
		except Exception as e:
			print("Error, please select choice Y/n")
			status_rsp=False

		while status_rsp == False:
			rsp=input(" Select path of the files  Y/n:\n")


			try:
				if rsp=="y" or rsp=="Y" or rsp=="yes" or rsp =="YES" or rsp =="n" or rsp=="N" or rsp=="not" or rsp=="NOT":
					status_rsp=True
			except Exception as e:
				print("Error, please select choice Y/n")
				status_rsp=False
		


		if rsp=="y" or rsp=="Y" or rsp=="yes" or rsp =="YES":
			self.Pregunta_yes()
		else:
			self.Pregunta_not()

	def Lec_files(self,a):
		

		files_all=a
		print ("recibe los valores: ", files_all)

		#files_all=['/home/carlos/Dropbox/Catalogo_2006_2007/Phases_2006/','/home/carlos/Dropbox/Catalogo_2006_2007/Phases_2007/']
		seq=0		
		path_archivos=[]
		for fi in files_all:
			file=fi
			archivos=os.listdir(file)

			archivos=sorted(archivos)
			#path_archivos+=archivos
			#print 'Archivos: \n', archivos
			aux=[]
			for arh in range(len(archivos)):
				i=archivos[arh]
				aux.append(file+i)
			path_archivos+=aux	

		print (path_archivos)
		print ("proceso datos proceso")
		self.datos_proceso(path_archivos)	
			
		#return path_archivos

	def Proceso(self, i, origen, model, path_salve):
		#funcion para las nuevas modificaciones
		#def Proceso(self, path, origen, model):
		print ("Ingresa al proceso Data times")
		#print path
		print (origen)
		print (model)
		

		inicio_read_file=Lectura_file()
		arreglo=[]
		arreglo=inicio_read_file.main(i)
		#print origen
		time_origen=origen[3]
		if time_origen[5]==' ':
			aux_o1=time_origen[0:4]
			aux_o2=time_origen[6:len(time_origen)]
			time_origen=aux_o1+'0'+aux_o2
			#print 'aqui', time_origen
		time_origen=time_origen.replace(' ', '')
		#print 'cambiado', time_origen
		origen_hora=int(time_origen[0:2])
		origen_min=int(time_origen[2:4])
		origen_seg=int(time_origen[len(time_origen)-5:len(time_origen)-3])
		origen_cen=float(time_origen[len(time_origen)-3:len(time_origen)])
		for a in arreglo:
			if len(a[0])==24:
				time_pick=(a[0][len(a[0])-9:len(a[0])])
				pick_hora=int(time_pick[0:2])
				pick_min=int(time_pick[2:4])
				pick_seg=int(time_pick[len(time_pick)-5:len(time_pick)-3])
				pick_cen=float(time_pick[len(time_pick)-3:len(time_pick)])
			else:
				time_pick=(a[0][15:24])
				#time_pick=float(a[0][len(a[0])-9:len(a[0])])
				pick_hora=int(time_pick[0:2])
				pick_min=int(time_pick[2:4])
				pick_seg=int(time_pick[len(time_pick)-5:len(time_pick)-3])
				pick_cen=float(time_pick[len(time_pick)-3:len(time_pick)])
			

			if pick_cen-origen_cen<0:
				#print 'menor en cen'
				result_cen=pick_cen+(0.99-origen_cen)
				pick_seg=pick_seg-1
			else:
				result_cen=pick_cen-origen_cen
			#print pick_seg
			if pick_seg-origen_seg<0:
				#print 'menor seg'
				result_seg=pick_seg+(59-origen_seg)
				pick_min=pick_min-1
			else:
				result_seg=pick_seg-origen_seg
			if pick_min- origen_min<0:
				#print 'menor min'
				result_min=pick_min+(59-origen_min)
				pick_hora-1
			else:
				result_min=pick_min- origen_min
			if pick_hora-origen_hora<0:
				#print 'menor hora'
				result_hora=pick_hora+(23-origen_hora)
			else:
				result_hora=pick_hora-origen_hora

			#print time_origen, time_pick
			#print a[0][0:4] , '---', pick_hora,pick_min,pick_seg, pick_cen, '---', origen_hora,origen_min,origen_seg, origen_cen,'---', result_hora, result_min, result_seg, result_cen
			#model='codex_13'
			# Verifica si la carpeta existe para guardar resultados, en otro caso genera l carpeta
			
			
			if not os.path.exists(path_salve):
				os.makedirs(path_salve)
			
			#fo = open('/home/carlos/Escritorio/datos_time/'+model+'/id_'+origen[0]+'.txt', 'a')
			fo = open(path_salve+'id_'+origen[0]+'.txt', 'a')
			if result_seg < 30 and float(origen[7]) < 2:
				time=result_seg+result_cen
				print (a[0][0:4], time, origen[7], 'P')
				fo.write(str(a[0][0:4])+'   '+str(time)+'\t'+str(origen[7])+'\n')


#inicio=file_hypodd()
#inicio.main()