

class RespuestaBash():
	def Main(self, estaciones, ruta_salve):
		
		fo = open(ruta_salve+'respuesta.bash', 'a')
		fo.write('#!/bin/bash\n')
		fo.write('FILES=`ls -1d *`\n')
		# pruebas de git
		fo.write('for FILE in $FILES\n')
		fo.write('do\n')
		fo.write('\tcd $FILE\n')
		fo.write('\techo $FILE\n')
		fo.write('\n')
		for i in estaciones:
			fo.write('\t\tmkdir '+i+'\n')
			fo.write('\t\tmv *'+i+'* '+i+'\n')
			fo.write('\t\tcd '+i)
			fo.write('\t\tcp ../../response_'+i+' .\n')
			fo.write('\t\tsac << sacend\n')
			fo.write('\t\t\tr *'+i+'*.SAC\n')
			fo.write('\t\t\tm response_'+i+'\n')
			fo.write('\t\tq\n')
			fo.write('sacend\n')
			fo.write('\t\trm response_'+i+'\n')
			fo.write('\t\tmv *'+i+'* ..\n')
			fo.write('\t\tcd ..\n')
			fo.write('\t\trmdir '+i+'\n')
      	
		fo.write('   cd ..\n')
		fo.write('done\n')



estaciones=[]
for i in range(55):

	if i<10:
		estaciones.append('MA0'+str(i+1))
	else:
		estaciones.append('MA'+str(i+1))

ruta_salve='/home/carlos/Documentos/crossquake/XF/Respuestasmars/'

inicio=RespuestaBash()
inicio.Main(estaciones, ruta_salve)