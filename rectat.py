import matplotlib.pyplot as plt
import numpy as np

# Punto específico
x1, y1 = 2, 3

# Ángulo en grados
theta_deg = 135

# Convertir el ángulo a radianes
theta_rad = np.radians(theta_deg)

# Calcular la pendiente
m = np.tan(theta_rad)

# Crear datos para la línea
x = np.linspace(-5, 5, 100)
y = m * (x - x1) + y1

# Dibujar el punto específico
plt.plot(x1, y1, 'ro', label='Punto Específico')

# Dibujar la línea
plt.plot(x, y, label='Línea a 135°')

# Configurar la gráfica
plt.axhline(0, color='black',linewidth=0.5)
plt.axvline(0, color='black',linewidth=0.5)
plt.grid(color = 'gray', linestyle = '--', linewidth = 0.5)
plt.title('Línea a 135° que Pasa por un Punto Específico')
plt.xlabel('Eje X')
plt.ylabel('Eje Y')
plt.legend()
plt.show()