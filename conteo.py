import os
import os.path
import numpy as np

class semillastations():
	def Main(self):
		#for e in range(len(responses)):
			#print(responses[e])
		file="/home/carlos/Documentos/CrossQuake-Data/tremores/corner_frequency_comple/"
		file22="/home/carlos/Documentos/CrossQuake-Data/tremores/"
		archivos=os.listdir(file)
		archivos=sorted(archivos)
		self.path=[]
		path11=[]
		self.histo=[]
		self.promedios=[]
		for i in archivos:
			ruta0=	file+i
			self.path.append(ruta0)
			path11.append(i)


		self.freq=[]
		self.Mo=[]	
		for i in range(len(self.path)):
			#print(self.path[i])
			ruta_archivo=self.path[i]
			# Abre el archivo en modo de lectura
			with open(ruta_archivo, "r") as archivo:
				# Lee todas las líneas del archivo y las almacena en una lista
				lineas = archivo.readlines()
				
			ele=[]
			for ii in lineas:
				elementos = ii.strip().split("\t")
				ele.append(elementos)
			

			for elemento in ele:
				#print(elemento)
				if elemento[0]=="average freq:":
					self.freq.append(round(float(elemento[1]), 1))
				if elemento[0]=="average Mo:":
					self.Mo.append(round(float(elemento[1]), 1))



					
		#print(self.freq)
		#print(len(self.freq))
		# Convertir a conjunto para obtener elementos únicos
		elementos_unicos=[]

		for ii in range(len(self.freq)):
			if self.freq[ii] not in elementos_unicos:
				elementos_unicos.append(self.freq[ii])

		elementos_unicos=sorted(elementos_unicos)
		print(elementos_unicos)
		print(len(elementos_unicos))



		freq_suma=np.zeros(len(elementos_unicos))
		cont_prom=np.zeros(len(elementos_unicos))
		mo_suma=np.zeros(len(elementos_unicos))
		freq_prom=np.zeros(len(elementos_unicos))
		mo_prom=np.zeros(len(elementos_unicos))
		for ii in range(len(elementos_unicos)):
			for j in range(len(self.freq)):
				if elementos_unicos[ii]==self.freq[j]:
					
					mo_suma[ii]=mo_suma[ii]+self.Mo[j]
					cont_prom[ii]=cont_prom[ii]+1


		for ii in range(len(elementos_unicos)):
			mo_prom[ii]=round((mo_suma[ii]/cont_prom[ii]), 1)




			
		fo = open(file22+'freq_freq.txt', 'a')
		total=0
		for i in range(len(elementos_unicos)):
			#fo.write(str(elementos_unicos[i])+"\n")
			fo.write(str(mo_prom[i])+"\t"+str(elementos_unicos[i])+"\t"+str(int(cont_prom[i]))+"\n")
			total=total+int(cont_prom[i])

		print("Total", total)





			# for linea in range(0, n):
			# 	sta_aux=lineas[linea][0:4]
				
			# 	freq_sta=lineas[linea][len(lineas[linea])-4: len(lineas[linea])]
			# 	print(sta_aux)
			# 	print(freq_sta)


			

			

estaciones=['ALPI', 'BAVA', 'CANO', 'CDGZ', 'COLM', 'COMA', 'CUAT','EBMG', 'ESPN', 'GARC','HIGA', 'JANU', 'MAZE', 'MORA', 'OLOT', 'PAVE', 'PERC', 'SANM', 'SCRI', 'SINN', 'SNID', 'ZAPO']
for i in range(56):

	if i<9:
		estaciones.append('MA0'+str(i+1))
	else:
		estaciones.append('MA'+str(i+1))
inicio = semillastations()
inicio.Main()