import obspy
import numpy as np

def calcular_momento_sismico(tr):
    # Obtener la traza de forma de onda sísmica
    data = tr.data
    
    # Obtener la información sobre la traza
    dt = tr.stats.delta
    rho = 2670  # Densidad típica de la corteza terrestre en kg/m^3
    vs = 3500   # Velocidad de corte típica en m/s
    A = 1e6     # Área típica del fallo en m^2
    sigma = 10e6  # Resistencia a la fractura típica en N/m^2
    
    # Calcular el momento sísmico
    momento_sismico = np.sum(data ** 2) * dt * rho * vs**2 * A * sigma
    momento_sismico=round(momento_sismico, 2)
    return momento_sismico

# Cargar el archivo SAC
archivo_sac = '/home/carlos/Documentos/CrossQuake-Data/tremores/2006.01.031.04/2006.031.09.18.26.ALPI.ZA.HHZ.SAC'
traza = obspy.read(archivo_sac)[0]

# Calcular el momento sísmico
momento = calcular_momento_sismico(traza)


print(f"El momento sísmico es: {momento} Nm")