ruta_archivo='/home/carlos/Documentos/CrossQuake-Data/tremores/Histograma.txt'
file='/home/carlos/Documentos/CrossQuake-Data/tremores/'
with open(ruta_archivo, "r") as archivo:
	# Lee todas las líneas del archivo y las almacena en una lista
	lineas = archivo.readlines()

datos=[]
for linea in lineas:
	x = linea.find(":")

	y=round(float(linea[0:x]),1)
	#y=(float(linea[0:x]))
	datos.append(y)

datos=sorted(datos)

# Crear un diccionario para almacenar las cuentas
conteo_valores = {}

# Contar la frecuencia de cada valor en la lista
for valor in datos:
	if valor in conteo_valores:
			conteo_valores[valor] += 1
	else:
		conteo_valores[valor] = 1

# Guardar los resultados en un archivo de texto
with open(file+"Histo.txt", "w") as archivo:
	for valor, cantidad in conteo_valores.items():
		archivo.write(f"{valor}: {cantidad} \n")