import obspy
import numpy as np
import matplotlib.pyplot as plt

def calcular_espectro(trace, delta):
    # Calcular el espectro de amplitud
    #f, Pxx = plt.psd(trace, NFFT=1024, Fs=1.0 / delta, color='black', linestyle='--')
    #print(np.sqrt(Pxx))
    #return f, np.sqrt(Pxx)


    t_start = 0  # tiempo de inicio en segundos
    t_end = trace.stats.endtime - trace.stats.starttime  # tiempo de fin en segundos
    npts = int(trace.stats.sampling_rate * t_end)  # Convertir a entero
    dt = 1 / trace.stats.sampling_rate
    f, Pxx = plt.psd(trace.data, NFFT=1024, Fs=1.0 / delta, color='black', linestyle='--')
    #plt.semilogx(f, Pxx, color='black', linestyle='--')
    # Calcular la transformada de Fourier
    freq, ampl = np.fft.fftfreq(npts, dt), np.fft.fft(trace.data)
    positive_freq_indices = np.where(freq >= 0)  # Índices de frecuencias positivas
        
    # Tomar solo la parte positiva de la transformada
    freq_pos, ampl_pos = freq[positive_freq_indices], ampl[positive_freq_indices]

    # Calcular la densidad espectral de potencia
    power_spectrum = np.abs(ampl_pos) ** 2

    #plt.loglog(power_spectrum)
    #plt.show()


    # Establecer un límite inferior en la frecuencia para graficar
    lower_freq_limit = 1.0  # Ajusta según tus necesidades

    # Suavizar el power_spectrum con un filtro de media móvil
    window_size = 150  # Tamaño de la ventana del filtro de media móvil
    power_spectrum_smooth = np.convolve(power_spectrum, np.ones(window_size)/window_size, mode='same')

    return freq_pos,  power_spectrum_smooth


def calcular_relacion_hv(traza_n, traza_e, traza_z):
    # Calcular los espectros de amplitud para las componentes horizontales y verticales
    f_n, A_n = calcular_espectro(traza_n, traza_n.stats.delta)
    f_e, A_e = calcular_espectro(traza_e, traza_e.stats.delta)
    f_z, A_z = calcular_espectro(traza_z, traza_z.stats.delta)
    
    # Calcular la relación H/V
    #relacion_hv = np.sqrt((A_n**2 + A_e**2) / A_z**2)
    n = [x**2 for x in A_n]
    e = [x**2 for x in A_e]
    z = [x**2 for x in A_z]
    suma=0
    suman=0
    sumae=0
    sumazz=0
    sumaz=0
    b=0
    for i in range(len(A_z)):
        #print(tr_n[i]**2, tr_e[i]**2)
        #print(tr_n[i]**2+tr_e[i]**2)
        a=(A_n[i]**2+A_e[i]**2)
        b=a+b
        suma=suma+((A_n[i]*A_n[i])+(A_e[i]*A_e[i]))
        suman=suman+(A_n[i]**2)
        sumae=sumae+(A_e[i]**2)
        sumazz=sumazz+(A_z[i]**2)
        sumaz=sumaz+(abs(A_z[i]))

    #print(b)
    #print(sumaz, suma)
    #res=np.sqrt(suma/sumaz)
    res=np.sqrt((suman+sumae)/sumazz)
    print(res, "****")
    suma = np.array(n)+np.array(e)
    hv=suma/np.array(z)
    relacion_hv=np.sqrt(hv)
    #print(relacion_hv)
    return f_n, relacion_hv


# Cargar las formas de onda de las tres componentes (N, E, Z)
archivo_sac_n = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.07.07.09/2006.07.07.09.ZAPO.HHN.SAC"
archivo_sac_e = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.07.07.09/2006.07.07.09.ZAPO.HHE.SAC"
archivo_sac_z = "/home/carlos/Documentos/CrossQuake-Data/tremores/2006.07.07.09/2006.07.07.09.ZAPO.HHZ.SAC"

traza_n = obspy.read(archivo_sac_n)[0]
traza_e = obspy.read(archivo_sac_e)[0]
traza_z = obspy.read(archivo_sac_z)[0]

# Calcular la relación H/V
f_hv, relacion_hv = calcular_relacion_hv(traza_n, traza_e, traza_z)

# Graficar la relación H/V
plt.figure(figsize=(10, 6))
plt.semilogx( f_hv, relacion_hv, color='black', linestyle='--', label='Relación H/V')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Relación H/V')
plt.legend()
plt.grid(True, which='both', linestyle='--', linewidth=0.5)
plt.show()