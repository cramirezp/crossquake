To run CrossQuake you can do it from a Docker container

1.- Install  Docker

    https://www.docker.com/

2.- Start  Docker and run : 

    docker pull cramzp/crossquake:tag

3.- Check:

    docker images

4.- Run container of docker with crossquake

    docker run -it cramzp/crossquake:tag bash

6.- Download last version crossquake
    git clone https://gitlab.com/cramirezp/crossquake.git

7.- Run  scrip crossquake

    cd crossquake

    pipenv shell

    python Main.py

8.- Exit

     exit+enter

Note to run the container again it is only necessary to perform the following steps

1.- docker ps -a

2.- docker start xxxxxxxx(CONTAINER ID)

3.- docker exec -it xxxxxxx(CONTAINER ID) bash

4.- cd crossquake

    pipenv shell

    python Main.py

5.- Exit 
    exit+enter
